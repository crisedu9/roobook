import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:robook/app/core/api/api_route.dart';
import 'package:robook/app/core/constant/server_constant.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/core/exceptions/exceptions.dart';
import 'package:robook/app/core/infraestructure/remote_datasource.dart';
import 'package:robook/app/core/infraestructure/remote_datasource_types.dart';
import 'package:robook/app/data/models/book/book_search_doc_base.dart';
import 'package:robook/app/data/models/response_api.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/models/title_model.dart';
import 'package:robook/app/modules/home/domain/repository/book_repository.dart';

@Injectable(as: GetBookRepository)
class BookRepositoryImpl implements GetBookRepository {
  BookRepositoryImpl(@Named(RemoteDataSourceTypes.HTTP_DATASOURCE) this._api);

  final RemoteDataSource _api;

  @override
  Future<Either<Failure, List<BookDom>>>? getBooks(TitleDom titleDom) async {
    try {
      final ResponseApi responseBook = await _api.get(
          enpoint: ApiRouteConfig.apiUrl,
          params: {"q": titleDom.title}).timeout(const Duration(minutes: 1));
      final List<SearchDoc> books =
          SearchDoc.listFromJson(responseBook.data['docs']);
      final List<BookDom> booksearchDocBase = SearchDoc.toBookDom(books);
      return Right<Failure, List<BookDom>>(booksearchDocBase);
    } on TimeoutException catch (_) {
      log(_.toString());
      return Failure.error(ServerConstant.TIMEOUT_EXCEPTION, _.message, ex: _);
    } on SocketException catch (_) {
      log(_.toString());
      return Failure.error(ServerConstant.SOCKET_EXCEPTION, _.message, ex: _);
    } on ApiException catch (_) {
      log(_.toString());
      return Failure.error(
          _.getErrorApi()?.messageKey, _.getErrorApi()?.message,
          ex: _);
    } catch (e) {
      log('Fallo al Buscar el libro :( $e');
      return Failure.error(
        ServerConstant.TIMEOUT_EXCEPTION,
        'fallo al Buscar el libro',
      );
    }
  }
}
