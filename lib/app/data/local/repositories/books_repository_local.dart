import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/data/local/book_singleton.dart';

import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/repository/local/book_repository_local.dart';

@Injectable(as: BookRepository)
class BooksLocalImpl implements BookRepository {
  BooksLocalImpl();

  List<BookDom> dbBooksFavorites = <BookDom>[];

  @override
  Future<Either<Failure, List<BookDom>>?> addFavoritesBooks(
      BookDom book) async {
    if (dbBooksFavorites.contains(book)) {
      dbBooksFavorites.remove(book);
    }
    book.favorite.sink(true);
    dbBooksFavorites.add(book);
    BookSingleton().setBooks(dbBooksFavorites);
    return Right<Failure, List<BookDom>>(dbBooksFavorites);
  }

  @override
  Future<Either<Failure, List<BookDom>>?> getFavoritesBooks() async {
    // TODO: implement getFavoritesBooks

    return Right<Failure, List<BookDom>>(dbBooksFavorites);
  }

  @override
  Future<Either<Failure, List<BookDom>>?> deleteFavoriteBooks(
      BookDom book) async {
    // TODO: implement deleteFavoriteBooks
    book.favorite.sink(false);
    final List<BookDom> result = BookSingleton().getBooksFavorites;
    result.removeWhere((element) => element == book);
    dbBooksFavorites = result;
    return Right<Failure, List<BookDom>>(dbBooksFavorites);
  }
}
