import 'package:robook/app/modules/home/domain/models/book_model.dart';

class BookSingleton {
  List<BookDom> _book = [];

  static final BookSingleton _singleton = BookSingleton._internal();
  factory BookSingleton() {
    if (_singleton._book.isEmpty) _singleton._book = _singleton._book;
    return _singleton;
  }
  BookSingleton._internal();

 List<BookDom> get getBooksFavorites {
   return _book;
 }
 void setBooks(List<BookDom> bookDoom){
   _book=bookDoom;
  }
}