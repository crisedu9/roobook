// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author_base.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Author _$AuthorFromJson(Map<String, dynamic> json) => Author(
      name: json['name'] as String? ?? "",
      personal_name: json['personal_name'] as String? ?? "",
    );

Map<String, dynamic> _$AuthorToJson(Author instance) => <String, dynamic>{
      'name': instance.name,
      'personal_name': instance.personal_name,
    };
