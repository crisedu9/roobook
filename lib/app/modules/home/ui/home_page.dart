import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';
import 'package:robook/app/modules/home/ui/widgets/app_bar.widget.dart';
import 'package:robook/app/modules/home/ui/widgets/bottom_bar.widget.dart';
import 'package:robook/app/modules/home/ui/widgets/list_book_favorite.dart';
import 'widgets/list_book.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final BookBloc bloc = BlocProvider.of<BookBloc>(context);
    return SafeArea(
      child: Scaffold(
        appBar:  AppbarWidget(bloc:bloc),
        bottomNavigationBar: BottomNav(bloc, actIndex: 0),
        body:  CreateBody(bloc),
      ),
    );
  }
}

class CreateBody extends StatelessWidget {
 const  CreateBody(this.bloc,{Key? key}) : super(key: key);
final BookBloc bloc;


  @override
  Widget build(BuildContext context) {

    List<Widget> widgetsNavigationBottom = <Widget>[
      const ListBook(valid: true,),
      const ListBookFavorite(valid: false),
    ];
    // TODO: implement build
    return StreamBuilder<int?>(
        stream: bloc.blocIndxBottomBar.stream,
        builder: (BuildContext context, AsyncSnapshot<int?> snapshot) {
          return Container(
            decoration: const BoxDecoration(color: Colors.black),
            child: IndexedStack(
              index: snapshot.hasData ? snapshot.data : 0,
              children: widgetsNavigationBottom,
            ),
          );
        });
  }
}


