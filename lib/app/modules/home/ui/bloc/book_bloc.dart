import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:robook/app/core/bloc/generic_field_bloc.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/core/usecase/query.dart';
import 'package:robook/app/modules/home/application/commands/books_add_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_delete_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_remote_usecase.dart';
import 'package:robook/app/modules/home/application/queries/get_book_favorite_usecase.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/models/title_model.dart';
import 'book_state.dart';

class BookBloc extends Cubit<BookState> {
  BookBloc(
      {required this.bookRemoteUsecase,
      required this.bookAddRemoteUsecase,
      required this.getBookFavoritesUseCase,
      required this.bookDeleteRemoteUsecase})
      : super(BookStateInit()) {}
  final BookRemoteUsecase bookRemoteUsecase;
  final BookAddRemoteUsecase bookAddRemoteUsecase;
  final GetBookFavoritesUseCase getBookFavoritesUseCase;
final BookDeleteRemoteUsecase bookDeleteRemoteUsecase;

  void init() {}

  final GenericFieldBloc<int> blocIndxBottomBar =
      GenericFieldBloc<int>(defaultValue: 0);
  void findingBook(String title) async {
    emit(BookStateLoading());
    final Either<Failure, List<BookDom>>? response =
        await bookRemoteUsecase.execute(TitleDom(title: title));
    response?.fold((Failure failure) {
      emit(BookStateError(failure));
    }, (List<BookDom> r) {
      emit(BookStateSucces(r));
    });
  }

  Future<void> addBook(BookDom dom) async {
    final Either<Failure,List<BookDom>? >? response =
        await bookAddRemoteUsecase.execute(dom);
    response?.fold((Failure failure) {
      emit(state.setBookStateGetBooksFavoritesError(failure));
    }, (List<BookDom>? r) {
      emit(state.setBookStateGetBooksFavorites(r));
    });
  }

  Future<void> removeFavoriteBook(BookDom dom) async {
    final Either<Failure,List<BookDom>?  >? response =
    await bookDeleteRemoteUsecase.execute(dom);
    response?.fold((Failure failure) {
      emit(state.setBookStateGetBooksFavoritesError(failure));
    }, (List<BookDom>? r) {
      emit(state.setBookStateGetBooksFavorites(r));
    });
  }

}
