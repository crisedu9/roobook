import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/data/local/book_singleton.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';

class BookState {
  BookState(this.list, this.listFavorities);

  final List<BookDom>? list;
  final List<BookDom>? listFavorities;

  BookState setBookStateGetBooksFavorites(List<BookDom>? listFavorities) {
    return BookStateGetBooksFavorites(list, listFavorities);
  }

  BookState setBookStateGetBooksFavoritesError(Failure failure) {
    return BookStateAddErrorBook(failure, list);
  }
}

class BookStateInit extends BookState {
  BookStateInit() : super([], []);
}

class BookStateSucces extends BookState {
  BookStateSucces(List<BookDom>? list)
      : super(list, [...BookSingleton().getBooksFavorites]);
}

class BookStateError extends BookState {
  BookStateError(this.failure) : super([], []);
  final Failure failure;
}

class BookStateLoading extends BookState {
  BookStateLoading() : super([], []);
}

class BookStateFALoading extends BookState {
  BookStateFALoading() : super([], []);
}

class BookStateAddErrorBook extends BookState {
  BookStateAddErrorBook(this.failure, List<BookDom>? list) : super(list, []);
  final Failure failure;
}

class BookStateGetBooksFavorites extends BookState {
  BookStateGetBooksFavorites(List<BookDom>? list, this.listFavorities)
      : super(list, listFavorities);
  final List<BookDom>? listFavorities;
}

class BookStateDeleteBook extends BookState {
  BookStateDeleteBook(List<BookDom>? list, this.listFavorities)
      : super(list, listFavorities);
  final List<BookDom>? listFavorities;
}
