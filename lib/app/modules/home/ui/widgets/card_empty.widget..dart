import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class CardEmpty extends StatelessWidget {
  bool valid;

 CardEmpty(this.valid);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        borderRadius:
                        const BorderRadius.all(Radius.circular(15.0)),
                        border:
                        Border.all(width: 2.0, color: Colors.amberAccent)),
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      color: Colors.amberAccent,
                      height: 120,
                      width: 300,
                      child: Center(
                        child: Text(
                          valid
                              ? "!No tienes Libros¡"
                              : "¡No tiene Favoritos!",
                          style: TextStyle(
                            //color: Colors.black,
                              fontSize: 25,
                              fontWeight:  FontWeight.w900,
                              foreground: Paint()
                                ..shader = const LinearGradient(
                                  colors: <Color>[
                                    Colors.white70,
                                    Colors.white
                                  ],
                                ).createShader( const Rect.fromLTWH(100, 0, 200, 0))
                          ),
                        ),
                      ),
                    ),
                  )),
            ],
          ),
          SizedBox(
              height: 320, child: Lottie.asset('assets/47784-books.json'))
        ],
      ),
    );
  }
}
