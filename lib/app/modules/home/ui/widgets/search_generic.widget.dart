import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:robook/app/core/utils/components_app_theme.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';
import 'package:robook/app/modules/home/ui/bloc/book_state.dart';




class StringFieldValidator {
  static String? validate(String value) {
    return value.isEmpty ? 'String can\'t be empty' : null;
  }
}

class SearchGeneric extends StatelessWidget {
   SearchGeneric() ;

  Widget build(BuildContext context) {
    final BookBloc bookBloc =  BlocProvider.of<BookBloc>(context);
    return BlocBuilder<BookBloc,BookState>(
      builder:(BuildContext context, BookState state) {
        return TextFormField(
          key: const Key('Busqueda'),
          style: const TextStyle(color: Colors.white),
          onTap: () {},
          onChanged: (title) {},
          onFieldSubmitted: (title) {
            bookBloc.findingBook(title);
          },
          validator: (title) => StringFieldValidator.validate(title!),
          decoration: InputDecoration(
            contentPadding:
            const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
            border: InputBorder.none,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            suffixIcon: const Icon(Icons.search, color: Colors.white,),
            hintStyle: TextStyle(color: Colors.grey[400]),
            hintText: 'Busqueda...',
          ),
        );
      });
  }
}
