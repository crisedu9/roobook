import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:robook/app/core/utils/animation_transition.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';
import 'package:robook/app/modules/home/ui/bloc/book_state.dart';
import 'package:robook/app/routes/app_route.dart';
import 'package:robook/main.dart';

import 'card_empty.widget..dart';

class ListBook extends StatelessWidget {
  const ListBook({
    required this.valid,
  });

  final bool valid;

  @override
  Widget build(BuildContext context) {
    final BookBloc blocBook = context.read<BookBloc>();
    return BlocBuilder<BookBloc, BookState>(
        builder: (BuildContext context, BookState state) {
      if (state is BookStateLoading) {
        return const Center(child: CircularProgressIndicator());
      }
      if (state is BookStateError) {
        return const Center(child: Text('Fallo al traer libro'));
      } else if (state is BookStateSucces ||
          state is BookStateGetBooksFavorites) {
        return ListView.builder(
          addRepaintBoundaries: false,
          addAutomaticKeepAlives: false,
          padding: const EdgeInsets.only(bottom: 30, top: 20),
          itemCount: state.list!.length,
          itemBuilder: (context, i) =>
              _createItem(context, state.list![i], blocBook),
        );
      } else {
        return CardEmpty(valid);
      }
    });
  }
}

Widget _createItem(
  BuildContext context,
  BookDom book,
  BookBloc blocBook,
) {
  return Card(
    color: const Color.fromRGBO(128, 128, 128, 0.2),
    shadowColor: Colors.grey,
    elevation: 20,
    child: GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          CustomNamedPageTransition(MyApp.mtAppKey, AppRoutes.DETAIL,
              arguments: book),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        width: 150,
        height: 150,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 4,
              child: FadeInImage.assetNetwork(
                  fadeInDuration: const Duration(milliseconds: 200),
                  placeholder: 'assets/book.png',
                  imageErrorBuilder: (ctx, _, star) =>
                      Image.asset('assets/book.png'),
                  image:
                      'http://covers.openlibrary.org/b/id/${book.coverI.toString()}-L.jpg?default=false',
                  width: 190),
            ),
            Flexible(
              flex: 8,
              child: Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        book.title!.length > 50
                            ? "${book.title!.substring(0, 47)}..."
                            : book.title!,
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    Text(book.authorName ?? '',
                        style: const TextStyle(
                            color: Colors.white,
                            overflow: TextOverflow.ellipsis)),
                    Text(
                        book.firstPublishYear != null
                            ? book.firstPublishYear.toString()
                            : '',
                        style: const TextStyle(color: Colors.white)),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 4,
              child: BlocBuilder<BookBloc, BookState>(
                  builder: (BuildContext context, BookState state) {
                return StreamBuilder<bool?>(
                    stream: book.favorite.stream,
                    builder: (context, snapshot) {
                      return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: const CircleBorder(),
                            padding: const EdgeInsets.all(10),
                            primary: Colors.amberAccent,
                            minimumSize: const Size(20, 21)),
                        child: Icon(
                          snapshot.hasData && snapshot.data!
                              ? Icons.star
                              : Icons.star_border,
                          color: true ? Colors.white : Colors.white,
                        ),
                        onPressed: () {
                          if (snapshot.hasData && snapshot.data!) {
                            blocBook.removeFavoriteBook(book);
                          }
                          else{
                            blocBook.addBook(book);
                          }

                        },
                      );
                    });
              }),
            )
          ],
        ),
      ),
    ),
  );
}
