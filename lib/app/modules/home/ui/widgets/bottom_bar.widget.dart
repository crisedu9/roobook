import 'package:flutter/material.dart';
import 'package:robook/app/core/utils/components_app_theme.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';

class BottomNav extends StatelessWidget {
  BottomNav(this.bloc, {int? actIndex});

  final BookBloc bloc;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int?>(
        stream: bloc.blocIndxBottomBar.stream,
        builder: (context, snapshot) {
          return Container(
        decoration: BoxDecoration(
              color: Colors.white24,
              borderRadius: BorderRadius.circular(40),
              boxShadow: const [
                BoxShadow(
                    offset: Offset(0, 10),
                    color: Color.fromRGBO(128, 128, 128, 0.2),
                    blurRadius: 10,spreadRadius: 0)
              ]),
            child: ClipRRect(
              borderRadius:const BorderRadius.only(
                 topLeft: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
              child: BottomNavigationBar(

                iconSize: 20,
                type:BottomNavigationBarType.fixed ,
                backgroundColor: const Color.fromRGBO(128, 128, 128, 0.2),
                selectedIconTheme:
                    const IconThemeData(color: Colors.amberAccent, size: 25),
                selectedItemColor: whiteColor,
                selectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
                unselectedIconTheme:  IconThemeData(
                  color: greyColor,
                ),
                unselectedItemColor: greyColor,
                elevation: 0,
                items: const [
                  BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.star), label: 'Favoritos'),
                ],
                //optional, default as 0
                onTap: (i) {
                  bloc.blocIndxBottomBar.sink(i);
                },
                currentIndex: snapshot.data ?? 0,
              ),
            ),
          );
        });
  }
}
