import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_lorem/flutter_lorem.dart';
import 'package:robook/app/core/utils/components_app_theme.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';

class BookDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as BookDom;
    const color= Color.fromRGBO(128, 128, 128, 0.2);

    return Scaffold(
      appBar: AppBar(
        backgroundColor:Colors.white60,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20).copyWith(bottom: 0),
        child: Card(
          elevation: 20,
          shadowColor: greyColor,
          color:color,
          child: Container(

            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    color: Color.fromRGBO(128, 128, 128, 0.2),
                    blurRadius: 10,spreadRadius: 0)
              ],
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                   FadeInImage.assetNetwork(
                      fit: BoxFit.fill,
                        fadeInDuration: const Duration(milliseconds: 200),
                        placeholder: 'assets/book.png',
                        imageErrorBuilder: (ctx, _, star) => Image.asset('assets/book.png') ,
                        image:
                        'http://covers.openlibrary.org/b/id/${args.coverI.toString()}-L.jpg?default=false',
                        height: 200,
                        width: double.infinity),
                  const SizedBox(
                    height: 10,
                  ), //SizedBox
                  Container(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                      args.title!.length > 50
                          ? "${args.title!.substring(0, 47)}..."
                          : args.title!,
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        overflow:TextOverflow.ellipsis,
                        fontWeight: FontWeight.w500,
                      ), //Textstyle
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                      "${args.authorName!=null? args.authorName![0]:''} - ${args.firstPublishYear != null ? args.firstPublishYear!.toString() : ''}",
                      style: const TextStyle(
                        fontSize: 15,
                        overflow:TextOverflow.ellipsis,
                        color: Colors.white,
                      ), //Textstyle
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(25),
                    child: Text(
                      lorem(paragraphs: 1, words: 20),
                      style: const TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ), //Textstyle
                    ),
                  ),
                ],
              ),
            ),
          ), //SizedBox
        ),
      ),
    );
  }
}
