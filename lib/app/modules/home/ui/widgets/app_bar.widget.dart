import 'package:flutter/material.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';
import 'package:robook/app/modules/home/ui/widgets/search_generic.widget.dart';

class AppbarWidget extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(60);

  const AppbarWidget({Key? key, required this.bloc}) : super(key: key);
  final BookBloc bloc;

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
        preferredSize: preferredSize,
        child: StreamBuilder<int?>(
            stream: bloc.blocIndxBottomBar.stream,
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data == 1) {
                return Container(
                  width: 0,
                  height: 0,
                  padding: const EdgeInsets.all(0),
                );
              }
              return AppBar(
                backgroundColor: Colors.transparent,
                actions: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        width: 300,
                        margin: const EdgeInsets.only(right: 20, left: 20),
                        decoration: BoxDecoration(
                            color: Colors.white24,
                            borderRadius: BorderRadius.circular(40),
                            boxShadow: const [
                              BoxShadow(
                                  offset: Offset(0, 10),
                                  color: Color.fromRGBO(128, 128, 128, 0.2),
                                  blurRadius: 10)
                            ]),
                        child: SearchGeneric(),
                      ),
                    ],
                  )
                ],
              );
            }));
  }
}
