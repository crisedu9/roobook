import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/core/usecase/query.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/repository/local/book_repository_local.dart';
@injectable
class GetBookFavoritesUseCase
    extends Query<Either<Failure, List<BookDom>>, NoParams> {
  GetBookFavoritesUseCase(this.getBookRepositoryLocal);

  final BookRepository getBookRepositoryLocal;

  @override
  Future<Either<Failure, List<BookDom>>?> execute(NoParams params) async {
    return getBookRepositoryLocal.getFavoritesBooks();
  }
}
