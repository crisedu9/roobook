import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/core/usecase/command.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/models/title_model.dart';
import 'package:robook/app/modules/home/domain/repository/book_repository.dart';

@injectable
class BookRemoteUsecase extends Command<Either<Failure, List<BookDom>>, TitleDom> {
BookRemoteUsecase(this.getBookRepository);
final GetBookRepository getBookRepository;

  @override
  Future<Either<Failure,List<BookDom>>?> execute(TitleDom params) async {
  return getBookRepository.getBooks(params);
  }

}