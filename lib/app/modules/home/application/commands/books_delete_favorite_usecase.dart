import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/core/usecase/command.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/repository/local/book_repository_local.dart';

@injectable
class BookDeleteRemoteUsecase extends Command<Either<Failure,List<BookDom>? >, BookDom> {
  BookDeleteRemoteUsecase(this.deleteBookLocal);
  final BookRepository deleteBookLocal;

  @override
  Future<Either<Failure, List<BookDom>?>?> execute(BookDom params) async{
    // TODO: implement execute
    return deleteBookLocal.deleteFavoriteBooks(params);
  }


}