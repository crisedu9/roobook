import 'package:robook/app/core/bloc/generic_field_bloc.dart';

class BookDom {
  BookDom({this.title, this.authorName, this.firstPublishYear, this.coverI});

  String? title;
  String? authorName;
  int? firstPublishYear;
  GenericFieldBloc<bool> favorite =GenericFieldBloc<bool>(defaultValue: false);
  int? coverI;
}
