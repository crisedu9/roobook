import 'package:dartz/dartz.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
abstract class BookRepository{
  Future<Either<Failure, List<BookDom>>?> getFavoritesBooks();
  Future<Either<Failure, List<BookDom>>?> addFavoritesBooks(BookDom book);
  Future<Either<Failure,  List<BookDom>>?> deleteFavoriteBooks(BookDom book);
}