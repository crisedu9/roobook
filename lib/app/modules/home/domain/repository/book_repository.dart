import 'package:dartz/dartz.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/models/title_model.dart';


abstract class GetBookRepository{
 Future<Either<Failure,List<BookDom>>>? getBooks(TitleDom title);
}