
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:robook/app/data/local/books_local.dart';
import 'package:robook/app/modules/home/application/commands/books_add_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_remote_usecase.dart';
import 'package:robook/app/modules/home/application/queries/get_book_favorite_usecase.dart';
import 'package:robook/app/modules/home/domain/repository/book_repository.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';
import 'injection.config.dart';

final GetIt getItApp = GetIt.instance;

@injectableInit
void configureAppInjection() {
  unregisterServices();
  registerCustoms();
  $initGetIt(getItApp);
}

void registerCustoms() {
  getItApp.registerLazySingleton(() => BookBloc(bookRemoteUsecase: getItApp(),getBookFavoritesUseCase:getItApp() ,bookAddRemoteUsecase:getItApp(), bookDeleteRemoteUsecase: getItApp() ));
 getItApp.registerLazySingleton(() => Client());
 // getItApp.registerLazySingleton(() => GetBookRepository(getItApp()));
}

void unregisterServices() {
  // Services
  removeRegistrationIfExists<BookRemoteUsecase>();
  removeRegistrationIfExists<GetBookFavoritesUseCase>();
  removeRegistrationIfExists<BookAddRemoteUsecase>();
  removeRegistrationIfExists<GetBookRepository>();
}

void removeRegistrationIfExists<T extends Object>({String? instanceName}) {
  if (getItApp.isRegistered<T>(instanceName: instanceName)) {
    getItApp.unregister<T>(instanceName: instanceName);
  }
}
