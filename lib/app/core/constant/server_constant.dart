class ServerConstant {
  static const String TIMEOUT_EXCEPTION = 'TIMEOUT_EXCEPTION';
  static const String SOCKET_EXCEPTION = 'SOCKET_EXCEPTION';
  static const String INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';
  static const String UNKNOW_ERROR = 'UNKNOW_ERROR';
}
