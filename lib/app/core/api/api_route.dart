import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

mixin ApiRouteConfig {
  static String get host => dotenv.get('HOST', fallback: null);
  static String get apiUrl => dotenv.get('API_URL', fallback: null);
  static Future<void> loadEnviroments() async {
    if(kReleaseMode) {
      await dotenv.load(fileName: '.env.development');
    } else {
      await dotenv.load(fileName: '.env.development');
    }
  }
}
