import 'dart:async';
import 'dart:developer';
import 'package:robook/app/core/bloc/generic_field_bloc.dart';
import 'package:robook/app/core/utils/extension_utils.dart';
import 'package:rxdart/rxdart.dart';

class FormGroupBloc {
  FormGroupBloc({
    this.blocs,
    this.initialVerification= false}):assert(blocs!=null && blocs.isNotEmpty,'The "blocs" property cannot be null or empty')
  {
    //
    //  Initialization of the internal Stream controller
    //
    _controller = BehaviorSubject<bool>();
    _listenBlocs(blocs);
  }

  GenericFieldBloc<T>? getBloc<T>(String key){
    try{
      return blocs![key]! as GenericFieldBloc<T>;
    }catch(e){
      log('key no exist $e');
      return null;
    }
  }
  //
  // Stream which emits the actual validation result, combining
  // all the streams involved in the validation
  //
  final bool initialVerification;
  final Map<String, GenericFieldBloc<dynamic>>? blocs;
  late  BehaviorSubject<bool> _controller;

  //
  // Method which initializes the validation
  //
  void _listenBlocs(Map<String, GenericFieldBloc<dynamic>>? _blocs) {  
    // For each of the streams to be considered, listen
    // to the type of information they are going to emit
    //
    _blocs?.forEach((String key, GenericFieldBloc<dynamic> value) {
      _blocs[key]!.stream.listen(
        (dynamic data) {
          _blocs[key]!.hasErrorSink(false);
          // Data was emitted => this is not an error
          _validate(_blocs);
        },
        onError: (dynamic _) {
          // Error was emitted
          _blocs[key]!.hasErrorSink(true);
          _validate(_blocs);
        },
      );
    });

    _blocs?.forEach((String key, GenericFieldBloc<dynamic> value) {
      if(_blocs[key]?.validator!=null){ //has validation
        _blocs[key]!.hasErrorSink(true);
      }
      //Initial Validation Fields
      if(initialVerification){
        if(_blocs[key]?.defaultValue==null){
          _blocs[key]!.sink(null);
        }
      }
    });

    _validate(_blocs);
  }

  //
  // Routine which simply checks whether there is still at least one
  // stream which does not satisfy the validation
  // Depending on this check, emits a positive or negative validation outcome
  //
  bool _validate(Map<String, GenericFieldBloc<dynamic>>? _blocs) {
    final bool hasNoErrors = _blocs?.values.firstWhereOrNull((GenericFieldBloc<dynamic> b) => b.hasErrorValue==true)==null;
    sinkValid(hasNoErrors);
    return hasNoErrors;
  }

  //
  // Public outcome of the validation
  //
  Stream<bool> get isValidStream => _controller.stream;
  bool get isValid => _controller.valueOrNull??false;
  Function(bool) get sinkValid => _controller.sink.add;

  ///Dispose
  void dispose() {
    blocs?.forEach((String key, GenericFieldBloc<dynamic> value) {
      blocs![key]!.dispose();
    });
    _controller.close();
  }
}