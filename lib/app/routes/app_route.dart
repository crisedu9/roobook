class AppRoutes {
  //ListBook
  static const String LIST = '/listBook';
  //detailBook
  static const String DETAIL = '/detailBook';
  //Homepage
  static const String HOME = '/home';
}
