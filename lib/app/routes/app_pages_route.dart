import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:robook/app/modules/home/application/commands/books_add_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_delete_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_remote_usecase.dart';
import 'package:robook/app/modules/home/application/queries/get_book_favorite_usecase.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';
import 'package:robook/app/modules/home/ui/home_page.dart';
import 'package:robook/app/di/injection.dart';
import 'package:robook/app/modules/home/ui/widgets/detail_book_page.dart';

import 'app_route.dart';

mixin AppPagesRoute {
  static Map<String, Widget Function(BuildContext)> getRoutes() {
    return <String, Widget Function(BuildContext)>{
      AppRoutes.HOME: (BuildContext context) => MultiBlocProvider(
            providers: <BlocProvider<dynamic>>[
              BlocProvider<BookBloc>(
                  child: const HomePage(),
                  create: (BuildContext context) => BookBloc(
                       bookDeleteRemoteUsecase: getItApp<BookDeleteRemoteUsecase>(),
                      bookRemoteUsecase: getItApp<BookRemoteUsecase>(),
                      getBookFavoritesUseCase:
                          getItApp<GetBookFavoritesUseCase>(),
                      bookAddRemoteUsecase: getItApp<BookAddRemoteUsecase>())),
              BlocProvider<BookBloc>(
                  child: const HomePage(),
                  create: (BuildContext context) => BookBloc(
                      bookDeleteRemoteUsecase: getItApp<BookDeleteRemoteUsecase>(),
                      bookRemoteUsecase: getItApp<BookRemoteUsecase>(),
                      getBookFavoritesUseCase:
                          getItApp<GetBookFavoritesUseCase>(),
                      bookAddRemoteUsecase: getItApp<BookAddRemoteUsecase>()))
            ],
            child: const HomePage(),
          ),
      AppRoutes.DETAIL: (BuildContext context) => BookDetail()
    };
  }

  static BlocProvider<T> getInstanceBloc<T extends BlocBase<dynamic>>(
      BuildContext context,
      {Widget? widget}) {
    return BlocProvider<T>.value(
      value: BlocProvider.of<T>(context),
      child: widget,
    );
  }

  static void navigateWithBloc<T extends BlocBase<dynamic>>(
      BuildContext context, Widget widget) {
    Navigator.push<T>(
      context,
      MaterialPageRoute<T>(
        builder: (BuildContext contextBuild) {
          return getInstanceBloc<T>(context, widget: widget);
        },
      ),
    );
  }

  static void navigateAndReplaceWithBloc<T extends BlocBase<dynamic>>(
      BuildContext context, Widget widget) {
    Navigator.pushReplacement<void, void>(
      context,
      MaterialPageRoute<T>(
        builder: (BuildContext contextBuild) {
          return getInstanceBloc<T>(context, widget: widget);
        },
      ),
    );
  }

  static void navigateMultiBloc(BuildContext context, Widget widget,
      List<BlocProvider<dynamic>> blocProviderList) {
    Navigator.push<MultiBlocProvider>(
      context,
      MaterialPageRoute<MultiBlocProvider>(
        builder: (BuildContext contextBuild) {
          return MultiBlocProvider(
            providers: blocProviderList,
            child: widget,
          );
        },
      ),
    );
  }
}
