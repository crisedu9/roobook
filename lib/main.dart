import 'package:flutter/material.dart';
import 'package:robook/app/routes/app_route.dart';

import 'app/core/api/api_route.dart';
import '/app/di/injection.dart' as di;
import 'app/core/utils/app_theme.dart';
import 'app/routes/app_pages_route.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
   di.configureAppInjection(); // Initialize the dependency injection container
  await ApiRouteConfig.loadEnviroments();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static GlobalKey mtAppKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'RoBook',
      key: MyApp.mtAppKey,
      debugShowCheckedModeBanner: false,
      routes: AppPagesRoute.getRoutes(),
      initialRoute: AppRoutes.HOME,
      theme: AppTheme.dark(),
      themeMode: ThemeMode.dark,
    );
  }
}
