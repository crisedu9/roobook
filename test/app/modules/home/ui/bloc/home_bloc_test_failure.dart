import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:robook/app/core/api/api_route.dart';
import 'package:robook/app/core/errors/failure.dart';
import 'package:robook/app/core/infraestructure/remote/http/http_datasource_impl.dart';
import 'package:robook/app/data/remote/books/book_repository_remote.dart';
import 'package:robook/app/di/injection.dart';
import 'package:robook/app/modules/home/application/commands/books_add_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_delete_favorite_usecase.dart';
import 'package:robook/app/modules/home/application/commands/books_remote_usecase.dart';
import 'package:robook/app/modules/home/application/queries/get_book_favorite_usecase.dart';
import 'package:robook/app/modules/home/domain/models/book_model.dart';
import 'package:robook/app/modules/home/domain/models/title_model.dart';
import 'package:robook/app/modules/home/domain/repository/book_repository.dart';
import 'package:robook/app/modules/home/domain/repository/local/book_repository_local.dart';
import 'package:robook/app/modules/home/ui/bloc/book_bloc.dart';

import 'home_bloc_test.mocks.dart';

@GenerateMocks(<Type>[BookRepository, Client, BookRemoteUsecase])
void main() {
  late BookRemoteUsecase bookRemoteUsecase;
  late BookAddRemoteUsecase bookAddRemoteUsecase;
  late GetBookFavoritesUseCase getBookFavoritesUseCase;
  late BookDeleteRemoteUsecase bookDeleteRemoteUsecase;
  late GetBookRepository getBookRepository;
  late HttpDataSourceImpl remoteDataSource;
  late BookBloc bloc;
  final MockClient client = MockClient();
  TestWidgetsFlutterBinding.ensureInitialized();
  //configureAppInjection();
  group('test book bloc ', () {
    setUpAll(() async {
      await ApiRouteConfig.loadEnviroments();
      bookAddRemoteUsecase = BookAddRemoteUsecase(MockBookRepository());
      getBookFavoritesUseCase = GetBookFavoritesUseCase(MockBookRepository());
      bookDeleteRemoteUsecase= BookDeleteRemoteUsecase(MockBookRepository());
      remoteDataSource = HttpDataSourceImpl(client);
      getBookRepository = BookRepositoryImpl(remoteDataSource);
      bookRemoteUsecase = BookRemoteUsecase(getBookRepository);
      bloc = BookBloc(
          bookDeleteRemoteUsecase:bookDeleteRemoteUsecase,
          bookRemoteUsecase: bookRemoteUsecase,
          getBookFavoritesUseCase: getBookFavoritesUseCase,
          bookAddRemoteUsecase: bookAddRemoteUsecase);
    });
    test('Test init Bloc instance', () => bloc.init());
    test('Test failure Getbooks ', () {
      final Failure failure = Failure('something went wrong');
      when(bookRemoteUsecase.execute(TitleDom(title: 'baron'))).thenAnswer(
              (_) async => Future<Either<Failure, List<BookDom>>>.value(
              Left<Failure, List<BookDom>>(failure)));
      bloc.findingBook('carlos');
    });
  });
}
